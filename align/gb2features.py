#!/usr/bin/env python3

from Bio import SeqIO,GenBank
import argparse
import sys
import re

parser = argparse.ArgumentParser('covert')
parser.add_argument("gb")
parser.add_argument("--output",default=sys.stdout)

args = parser.parse_args()

if args.gb is None:
    sys.exit()

with open(args.gb,'r') as f:
    for seq_record in GenBank.parse(f):
        for i in seq_record.features:
            if i.key != "primer":
                for j in i.qualifiers:
                    if j.key == "/label=":
                        name = j.value
                print(name,end=",")
                print(re.sub("\.\.",",",re.sub("[\(\)]","",re.sub("complement","",i.location))),end=",")
                if re.match("complement",i.location) is not None:
                    print("-")
                else:
                    print("+")

