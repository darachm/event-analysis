#!/usr/bin/env python3

import argparse
import sys
import pysam
from Bio import SeqIO

parser = argparse.ArgumentParser('take sam break it up to detected breaks using cigars')
parser.add_argument('--sam',default=sys.stdin)
parser.add_argument('--ref',required=True)
args = parser.parse_args()

refs = dict()
for i in SeqIO.parse(args.ref,"fasta"):
    refs[i.id] = i

def get_state(tuple_of_pos):
    if tuple_of_pos[0] is not None:         # query
        if tuple_of_pos[1] is not None:     # ref
            return("M")
        else:
            return("I")
    else:
        return("D")

samfile = pysam.AlignmentFile(args.sam,"r")
for i in samfile:
    if not i.is_unmapped and not i.is_secondary and not i.is_supplementary:

        ref_name = samfile.get_reference_name(i.reference_id)
        ref_seq = refs[ref_name].seq
        query_seq = i.query_sequence

        pairs = i.get_aligned_pairs()
        if pairs[0][0] is None:
            state = "H"
        elif pairs[0][1] is None:
            state = "S"
        else:
            state = "M"

        events = []

        # Why all this and not cigar parsing? because that doesn't track 
        # single-position errors
        for which_pair, this_pair in enumerate(pairs):
            # this_pair is ( query_pos , ref_pos )
            # print(state,end="") # for debugging, print out states
            this_state = get_state(this_pair) # this is the current state M I D
            if state not in ['S','H']: # this is to help skip I D that's clipping
                if this_state != state: # 
                    #print(pairs[which_pair-1],pairs[which_pair],pairs[which_pair+1])
                    #print(state,this_state)
                    if state == 'M' and this_state == 'I': 
                        #print('insert start')
                        events.append((this_state,pairs[which_pair-1][1]+1+1)) 
                            # storing state and the previous ref pos but +1 ,
                            # to be similar to del this is where in ref it
                            # starts to insert...
                    elif state == 'M' and this_state == 'D': 
                        #print('del start')
                        events.append((this_state,this_pair[1]+1)) 
                            # storing state and ref pos of deletion start
                    elif state == 'I' and this_state == 'M': 
                        # print('insert end')
                        if events[len(events)-1][1] is not None:
                            events[len(events)-1] = ( events[len(events)-1] + 
                                    (this_pair[1] + 1 + 1 - 
                                        events[len(events)-1][1]
                                        ,) )
                        # storing length
                    elif state == 'D' and this_state == 'M': 
                        # print('del end')
                        if events[len(events)-1][1] is not None:
                            events[len(events)-1] = ( events[len(events)-1] + 
                                    (this_pair[1] + 1  -
                                        events[len(events)-1][1]
                                        ,) )
                        # storing length
                elif state == 'M': # but if it is matched, check for errors
                    if query_seq[this_pair[0]] != ref_seq[this_pair[1]] :
                        events.append(("X",this_pair[1]+1,
                                query_seq[this_pair[0]]+str(this_pair[1]+1)+
                                    ref_seq[this_pair[1]]
                                )) 
                        # storing state, ref pos, and the two bases
                state = this_state
            elif state == 'M': # then we can begin the not clipping part
                state = this_state
        if state in ['I','D']:
            try:
                events.pop() # tidy up at end, so no clipping
            except:
                pass

        print("-   type: alignment")
        print("    query: "+i.query_name)
        print("    ref: "+ref_name)
        print("    events: ")
        for which_event, event in enumerate(events):
            if event[0] == 'X':
                print("        - type: "+event[0])
                print("          pos: "+str(event[1]))
                print("          mutation: "+event[2])
            else:
                print("        - type: "+event[0])
                print("          pos: "+str(event[1]))
                print("          length: "+str(event[2]))
        print()

