#!/usr/bin/env python3

import argparse
import sys
import yaml
from Bio import SeqIO,pairwise2
import numpy as np

parser = argparse.ArgumentParser('take sam break it up to detected breaks using cigars')
parser.add_argument('--events-yaml',default=sys.stdin)
parser.add_argument('--ref',required=True)
parser.add_argument('--size-filter',default=10,type=int)
parser.add_argument('--end-size',default=100,type=int)
parser.add_argument('--buffer-ends',default=10,type=int)
parser.add_argument('--threshold-score',default=180,type=int)
args = parser.parse_args()

refs = dict()
for i in SeqIO.parse(args.ref,"fasta"):
    refs[i.id] = {'seq': str(i.seq), 'events': []}

events = yaml.safe_load( args.events_yaml )
for i in events:
    if i['type'] == 'alignment':
        if i['events'] is not None:
            refs[i['ref']]['events'] += i['events']

def get_ends(seq,start,stop,end_size=args.end_size,buffer_ends=args.buffer_ends):
    return( { 
            'left_outer': seq[ (start-end_size):(start+buffer_ends) ] ,
            'left_inner': seq[ (start-buffer_ends):(start+end_size) ] ,
            'right_inner': seq[ (stop-end_size):(stop+buffer_ends) ] ,
            'right_outer': seq[ (stop-buffer_ends):(stop+end_size) ]
            } )

for ref in refs:

    refs[ref]['tracks'] = {
        'junction_deletion': np.zeros(len(refs[ref]['seq'])),
        'junction_insertion': np.zeros(len(refs[ref]['seq']))
        }
    refs[ref]['ends_deletion'] = []
    refs[ref]['ends_insertion'] = []

    for event in refs[ref]['events']:

        if event['type'] in ['D']:
            if event['length'] > args.size_filter:
                start = event['pos']
                stop  = event['pos']+event['length']
                refs[ref]['tracks']['junction_deletion'][start] += 1
                refs[ref]['tracks']['junction_deletion'][stop ] += 1
                refs[ref]['ends_deletion'].append( 
                    get_ends(refs[ref]['seq'],start,stop) )

        if event['type'] in ['I']:
            if event['length'] > args.size_filter:
                refs[ref]['tracks']['junction_insertion'][event['pos']] += 1
                refs[ref]['tracks']['junction_insertion'][event['pos']+event['length']] += 1
                refs[ref]['ends_insertion'].append( 
                    get_ends(refs[ref]['seq'],start,stop) )

    del refs[ref]['tracks'] # I dunno can't handle that shit yet


for ref in refs:
    print(ref)
    if ref != '9kb_easy_Hyg':
        pass 
    for deletion in refs[ref]['ends_deletion']:
        aln_params = (2, -5, -3, -3)
        l2roi = pairwise2.align.localms( 
                deletion['left_outer'],deletion['right_inner'] ,*aln_params,
                one_alignment_only=True)[0]
        r2loi = pairwise2.align.localms( 
                deletion['right_outer'],deletion['left_inner'] ,*aln_params,
                one_alignment_only=True)[0]
        l2roo = pairwise2.align.localms( 
                deletion['left_outer'],deletion['right_outer'] ,*aln_params,
                one_alignment_only=True)[0]
        r2loo = pairwise2.align.localms( 
                deletion['right_outer'],deletion['left_outer'] ,*aln_params,
                one_alignment_only=True)[0]

        alnz = {
                'l2roi': l2roi,
                'r2loi': r2loi,
                'l2roo': l2roo,
                'r2loo': r2loo,
                }

        for j in alnz:
            if alnz[j].score > args.threshold_score:
                print(j)
                print( pairwise2.format_alignment(*alnz[j]) )





#print(yaml.dump( refs) )


#    for i in refs[ref]['tracks']['junction_deletion']:
#        print(int(i),end="")


