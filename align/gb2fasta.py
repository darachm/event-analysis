#!/usr/bin/env python3

from Bio import SeqIO
import argparse
import sys

parser = argparse.ArgumentParser('covert')
parser.add_argument("gb")
parser.add_argument("--output",default=sys.stdout)

args = parser.parse_args()

if args.gb is None:
    sys.exit()

for seq_record in SeqIO.parse(args.gb,"genbank"):
    SeqIO.write(seq_record,args.output,"fasta")
