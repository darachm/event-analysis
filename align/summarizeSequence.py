#!/usr/bin/env python3

import argparse
import sys
import yaml
from Bio import SeqIO #,pairwise2
import numpy as np
import seqfold

parser = argparse.ArgumentParser('summarize seq to track')
parser.add_argument('--sequences',default=sys.stdin)
parser.add_argument('--file',default=None,type=chr)
parser.add_argument('--output',default=sys.stdout)
parser.add_argument('--gc-window',default=15,type=int)
parser.add_argument('--mfe-window',default=60,type=int)
parser.add_argument('--mfe-skip',default=10,type=int)
parser.add_argument('--mfe-offset',default=0,type=int)
parser.add_argument('-v','--verbose',action='count',default=0)
args = parser.parse_args()

if args.sequences is None:
    sys.exit()

def calcGC(sequence):
    return( float(sum([ np.sum(np.char.count(sequence,i)) for i in ('G','C') ])/len(sequence)) )

def calcGCtrack(sequence,window=9):
    print("Calculating GC track for window size",window,file=sys.stderr)
    pad = int(np.ceil(window/2))
    return( map( calcGC,
        np.lib.stride_tricks.sliding_window_view(
            sequence.seq[-(pad):-1]+sequence.seq+sequence.seq[0:(pad-1)] , 
                # so it's padding from the rest of the circular plasmid,
                # looking back pad-1 distance (so just -pad) because the
                # index is including the center one, then pad-1 ahead.
                # It matches hand calculations for each position at starts,
                # ends of test sequence.
            window)
        ))

def calcMFEtrack(sequence,window=101,skip=50,offset=0):
    print("Calculating MFE for",window,"base windows every",skip,"bases",file=sys.stderr)
    pad = int(np.ceil(window/2))
    return( map( lambda x: seqfold.dg("".join(x),temp=37),
        np.lib.stride_tricks.sliding_window_view(
            sequence[-(pad):-1]+sequence+sequence[0:(pad-1)] , 
            window)[offset::skip]
        ))


input_sequences = SeqIO.parse(args.sequences,"fasta")

output_tracks = dict()

for i in input_sequences:
    print("doing",i.id,file=sys.stderr)
    output_tracks[i.id] = dict()

    output_tracks[i.id]['sequence'] = str(i.seq).strip()

    gc_window = args.gc_window
    gc_name = 'gc_'+str(gc_window)
    output_tracks[i.id][gc_name] = list(calcGCtrack(i,gc_window))

    # MFE folding energy
    mfe_window = args.mfe_window
    mfe_skip = args.mfe_skip
    mfe_offset = args.mfe_offset
    mfe_name = 'mfe_'+str(mfe_window)+'_'+str(mfe_skip)+'_'+str(mfe_offset)
    output_tracks[i.id][mfe_name] = [ 'NA' for z in range(len(i.seq)) ]
    output_tracks[i.id][mfe_name][(mfe_offset)::mfe_skip] = \
        (list(
            calcMFEtrack(i.seq,mfe_window,skip=mfe_skip,offset=mfe_offset)
            ))

if args.verbose:
    for j in output_tracks:
        for k in output_tracks[j]:
            print(k,file=sys.stderr)
            print(output_tracks[j][k],file=sys.stderr)

if args.output is not sys.stdout:
    with open(args.output,'w') as f:
        yaml.dump( output_tracks , f )
else:
    print(yaml.dump(output_tracks))

#events = yaml.safe_load( args.events_yaml )

